package me.bumblebee.railminer.events;

import me.bumblebee.railminer.managers.Messages;
import me.bumblebee.railminer.utils.Protect;
import me.bumblebee.railminer.utils.ProtectedRegion;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

public class BlockPlace implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();

        for (List<ProtectedRegion> rList : Protect.prot.values()) {
            for (ProtectedRegion r : rList) {
                if (!r.isInside(e.getBlock().getLocation())) {
                    continue;
                }
                if (!r.canUse(p)) {
                    p.sendMessage(Messages.MINER_IS_PROTECTED.get());
                    e.setCancelled(true);
                    return;
                }
            }
        }
    }

}