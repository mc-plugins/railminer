package me.bumblebee.railminer.events;

import me.bumblebee.railminer.managers.UpgradeManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

public class PrepareItemCraft implements Listener {

    UpgradeManager upgrades = new UpgradeManager();

    @EventHandler
    public void onPrepareItemCraft(PrepareItemCraftEvent e) {
        //For Storage II - make better if adding more levels
        CraftingInventory inv = e.getInventory();
        int normalBooks = 0;
        int books = 0;
        int other = 0;
        for (int i = 0; i < inv.getMatrix().length - 1; i++) {
            ItemStack is = inv.getMatrix()[i];
            if (is == null)
                continue;
            if (is.getType() != Material.ENCHANTED_BOOK) {
                other++;
                continue;
            }
            normalBooks++;
            if (!is.hasItemMeta()) {
                other++;
                continue;
            }
            if (!is.getItemMeta().hasDisplayName()) {
                other++;
                continue;
            }

            YamlConfiguration c = upgrades.getYaml();
            ConfigurationSection recipes = c.getConfigurationSection("recipes");
            if (recipes == null)
                return;

            String storageItem = ChatColor.translateAlternateColorCodes('&', c.getString("recipes.storage.result"));
            if (is.getItemMeta().getDisplayName().equalsIgnoreCase(storageItem))
                books++;
            else
                other++;
        }

        if (normalBooks == 2) {
            if (books != 2 && other >= 0) {
                e.getInventory().setResult(new ItemStack(Material.AIR));
            }
        }
    }

}