package me.bumblebee.railminer.managers;

import me.bumblebee.railminer.RailMiner;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;

public class LimitManager {

    private static HashMap<UUID, Integer> running = new HashMap<>();

    public void setup() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "limits.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
            c.set("limits.blocks.0.limit", 10);
            c.set("limits.blocks.0.permission", "railminer.blimit.10");
            c.set("limits.blocks.1.limit", 20);
            c.set("limits.blocks.1.permission", "railminer.blimit.20");
            c.set("limits.miners.0.limit", 1);
            c.set("limits.miners.0.permission", "railminer.mlimit.1");
            c.set("limits.miners.1.limit", 10);
            c.set("limits.miners.1.permission", "railminer.mlimit.10");
            try {
                c.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public YamlConfiguration getBlockFile() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "limits.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
            c.set("limits.blocks.0.limit", 10);
            c.set("limits.blocks.0.permission", "railminer.blimit.10");
            c.set("limits.blocks.1.limit", 20);
            c.set("limits.blocks.1.permission", "railminer.blimit.20");
            try {
                c.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return YamlConfiguration.loadConfiguration(f);
    }

    public YamlConfiguration getMinerFile() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "limits.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
            c.set("limits.miners.0.limit", 1);
            c.set("limits.miners.0.permission", "railminer.mlimit.1");
            c.set("limits.miners.1.limit", 10);
            c.set("limits.miners.1.permission", "railminer.mlimit.10");
            try {
                c.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return YamlConfiguration.loadConfiguration(f);
    }

    public int getPlayerMinerLimit(Player p) {
        if (p.hasPermission("railminer.mlimit.admin"))
            return 0;
        if (getMinerFile().getConfigurationSection("limits.miners") == null)
            return 0;

        int limit = 0;
        Set<String> all = getBlockFile().getConfigurationSection("limits.miners").getKeys(false);
        for (String id : all) {
            int l = getBlockFile().getInt("limits.miners." + id + ".limit");
            if (p.hasPermission("railminer.mlimit." + l) && l > limit)
                limit = l;
        }
        return limit;
    }

    public HashMap<UUID, Integer> getRunning() { return running; }

    public void updateLimitFor(UUID uuid, boolean add) {
        int limit = 0;
        if (getRunning().containsKey(uuid))
            limit = getRunning().get(uuid);

        if (add)
            limit++;
        else
            limit--;

        getRunning().remove(uuid);
        getRunning().put(uuid, limit);
    }

    public int getAmountRunning(UUID uuid) {
        if (!running.containsKey(uuid))
            return 0;
        return running.get(uuid);
    }

    public int getPlayerBlockLimit(Player p) {
        if (p.hasPermission("railminer.blimit.admin"))
            return 0;
        if (getBlockFile().getConfigurationSection("limits.blocks") == null)
            return 0;

        int limit = 0;
        Set<String> all = getBlockFile().getConfigurationSection("limits.blocks").getKeys(false);
        for (String id : all) {
            int l = getBlockFile().getInt("limits.blocks." + id + ".limit");
            if (p.hasPermission("railminer.blimit." + l) && l > limit)
                limit = l;
        }
        return limit;
    }

}
