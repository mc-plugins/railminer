package me.bumblebee.railminer.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ProtectedRegion {

    UUID uuid;
    Location l1;
    Location l2;

    public ProtectedRegion(UUID uuid, Location p1, Location p2) {
        this.uuid = uuid;
        this.l1 = p1;
        this.l2 = p2;

        if (Protect.prot.containsKey(uuid)) {
            List<ProtectedRegion> p = Protect.prot.get(uuid);
            p.add(this);
            Protect.prot.put(uuid, p);
        } else {
            Protect.prot.put(uuid, new ArrayList<>(Collections.singleton(this)));
        }
    }

    public void remove() {
        List<ProtectedRegion> p = Protect.prot.get(uuid);
        Protect.prot.remove(uuid);
        if (p.size() > 1) {
            p.remove(this);
            Protect.prot.put(uuid, p);
        }
    }

    public boolean canUse(Player p) {
        if (p.getUniqueId().equals(uuid)) {
            return true;
        } else if (Friends.friends.containsKey(uuid)) {
            if (Friends.friends.get(uuid).contains(p.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public boolean isInside(Location loc) {
        int x1 = Math.min(l1.getBlockX(), l2.getBlockX());
        int z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
        int y1 = Math.min(l1.getBlockY(), l2.getBlockY());
        int x2 = Math.max(l1.getBlockX(), l2.getBlockX());
        int z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());
        int y2 = Math.max(l1.getBlockY(), l2.getBlockY());

        return loc.getX() >= x1 && loc.getX() <= x2 && loc.getZ() >= z1 && loc.getZ() <= z2 && loc.getY() >= y1 && loc.getY() <= y2;
    }



    public Player getOwner() { return Bukkit.getServer().getPlayer(uuid); }

    public Location getLocationOne() { return l1; }

    public Location getLocationTwo() { return l2; }

}
