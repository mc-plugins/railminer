package me.bumblebee.railminer.utils;

import me.bumblebee.railminer.Miner;
import me.bumblebee.railminer.RailMiner;
import me.bumblebee.railminer.managers.Messages;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class Protect {

    public static HashMap<UUID, List<ProtectedRegion>> prot = new HashMap<>();

    public void protectNewRegion(Player p, Miner m) {
        Location sign = m.getSignLocation();
        Location p1 = m.down(m.left(sign.clone()));
        Location p2 = m.forward(m.forward(m.forward(m.forward(m.forward(m.forward(m.forward(m.up(m.right(sign)))))))));
        new ProtectedRegion(p.getUniqueId(), p1, p2);
        p.sendMessage(Messages.MINER_PROTECTED.get());
    }

    public ProtectedRegion getRegion(Miner m) {
        Location sign = m.getSignLocation();
        Location p1 = m.down(m.left(sign.clone()));
        Location p2 = m.forward(m.forward(m.forward(m.forward(m.forward(m.forward(m.forward(m.up(m.right(sign)))))))));
        if (!prot.containsKey(m.getPlayer()))
            return null;
        for (ProtectedRegion r : prot.get(m.getPlayer())) {
            if (r.getLocationOne().equals(p1))
                if (r.getLocationTwo().equals(p2))
                    return r;
        }
        return null;
    }

    public void saveData() {
        YamlConfiguration c = getFile();
        c.set("protected", null);

        for (UUID uuid : Protect.prot.keySet()) {
            int i = 0;
            for (ProtectedRegion r : Protect.prot.get(uuid)) {
                Location l1 = r.getLocationOne();
                Location l2 = r.getLocationTwo();

                c.set("protected." + uuid + "." + i + ".one.world", l1.getWorld().getName());
                c.set("protected." + uuid + "." + i + ".one.x", l1.getBlockX());
                c.set("protected." + uuid + "." + i + ".one.y", l1.getBlockY());
                c.set("protected." + uuid + "." + i + ".one.z", l1.getBlockZ());
                c.set("protected." + uuid + "." + i + ".two.world", l2.getWorld().getName());
                c.set("protected." + uuid + "." + i + ".two.x", l2.getBlockX());
                c.set("protected." + uuid + "." + i + ".two.y", l2.getBlockY());
                c.set("protected." + uuid + "." + i + ".two.z", l2.getBlockZ());
                i++;
            }
        }

        saveFile(c);
    }

    public void loadData() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "protected.yml");
        if (!f.exists())
            return;
        YamlConfiguration c = YamlConfiguration.loadConfiguration(f);

        if (c.getConfigurationSection("protected") == null)
            return;

        Set<String> aProtected = c.getConfigurationSection("protected").getKeys(false);

        for (String s : aProtected) {
            if (c.getConfigurationSection("protected") == null)
                continue;

            Set<String> ids = c.getConfigurationSection("protected." + s).getKeys(false);
            UUID uuid = UUID.fromString(s);

            for (String id : ids) {
                World w1 = Bukkit.getServer().getWorld(c.getString("protected." + uuid + "." + id + ".one.world"));
                int x1 = c.getInt("protected." + uuid + "." + id + ".one.x");
                int y1 = c.getInt("protected." + uuid + "." + id + ".one.y");
                int z1 = c.getInt("protected." + uuid + "." + id + ".one.z");
                World w2 = Bukkit.getServer().getWorld(c.getString("protected." + uuid + "." + id + ".two.world"));
                int x2 = c.getInt("protected." + uuid + "." + id + ".two.x");
                int y2 = c.getInt("protected." + uuid + "." + id + ".two.y");
                int z2 = c.getInt("protected." + uuid + "." + id + ".two.z");

                Location l1 = new Location(w1, x1, y1, z1);
                Location l2 = new Location(w2, x2, y2, z2);
                new ProtectedRegion(uuid, l1, l2);
            }
        }
    }

    public YamlConfiguration getFile() {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "protected.yml");
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return YamlConfiguration.loadConfiguration(f);
    }

    public void saveFile(YamlConfiguration c) {
        File f = new File(RailMiner.getInstance().getDataFolder() + File.separator + "protected.yml");
        try {
            c.save(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
