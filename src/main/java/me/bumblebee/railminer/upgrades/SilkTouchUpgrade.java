package me.bumblebee.railminer.upgrades;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class SilkTouchUpgrade {

    Material m;
    public SilkTouchUpgrade(Material m) {
        this.m = m;
    }

    public ItemStack run() {
        return new ItemStack(m);
    }

}
