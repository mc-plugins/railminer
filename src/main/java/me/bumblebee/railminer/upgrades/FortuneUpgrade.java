package me.bumblebee.railminer.upgrades;

import java.util.Random;

public class FortuneUpgrade implements ReturnableUpgrade {

    @Override
    public Integer run() {
        Random random = new Random();
        return random.nextInt(100) <= 80 ? 2 : 3;
    }
}
