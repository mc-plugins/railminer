package me.bumblebee.railminer.upgrades;

public enum UpgradeType {

    LIQUID_PROTECTION("protection"),
    SPEED("speed"),
    FORTUNE("fortune"),
    STORAGE("storage"),
    STORAGE_II("storage_ii"),
    SILK_TOUCH("silk"),
    EFFICIENCY("efficiency"); //Less fuel usage

    String value;

    UpgradeType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static UpgradeType fromString(String s) {
        s = s.replace(" ", "_");
        for (UpgradeType u : UpgradeType.values()) {
            if (u.value.equalsIgnoreCase(s)) {
                return u;
            }
        }

        return null;
    }
}